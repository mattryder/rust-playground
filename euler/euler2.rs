fn main() {
  let mut ev_sum = 0u32;
  let mut _sum = 0u32;
  let mut a = 1u32;
  let mut b = 1u32;
  loop {
    _sum = a + b;
    b = a;
    a = _sum;

    if _sum % 2 == 0 {
      ev_sum += _sum;
    }

    if _sum >= 4_000_000 {
      println!("Answer: {}", ev_sum);
      break;
    }
  }
}